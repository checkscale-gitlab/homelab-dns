package main

import (
	"fmt"
	"github.com/cloudflare/cloudflare-go"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	homelabdns "homelab-dns"
	"log"
	"net"
	"os"
)

var cfgFile string
var cfApiKey string
var cfZoneId string

var rootCmd = &cobra.Command{
	Use:   "update",
	Short: "Check and update dns records",
	Long:  `Check if the current dns records point to the wrong IPs and update them via the cloudflare api`,
	Args:  cobra.MinimumNArgs(1),
	Run:   handleUpdate,
}

func handleUpdate(cmd *cobra.Command, args []string) {
	resolver := *homelabdns.NewResolver(net.LookupIP)

	api, err := cloudflare.NewWithAPIToken(cfApiKey)
	if err != nil {
		log.Fatal(err)
	}
	client := *homelabdns.NewClient(api, cfZoneId)
	app := homelabdns.NewDefaultApp(resolver, client)
	err = app.CheckAndUpdateRecords(cmd.Context(), args[0])
	if err != nil {
		log.Fatal(err)
	}
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.summarizer-cli.yaml)")
	rootCmd.PersistentFlags().StringVar(&cfApiKey, "key", "", "Cloudflare API Key")
	rootCmd.PersistentFlags().StringVar(&cfZoneId, "zone", "", "Cloudflare ZoneID")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".homelab-dns" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".homelab-dns")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}
