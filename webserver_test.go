package homelab_dns_test

import (
	"errors"
	"fmt"
	"github.com/cloudflare/cloudflare-go"
	homelab_dns "homelab-dns"
	"net"
	"net/http"
	"net/http/httptest"
	"testing"
)

func ExampleNewAppServer() {
	// Setup Spy to simulate cloudflare and lookup IP
	spyLookup := &homelab_dns.SpyLookup{
		ReturnValue: []net.IP{net.ParseIP("0.0.0.0")},
	}
	resolver := *homelab_dns.NewResolver(spyLookup.LookupIP)
	spyApi := &homelab_dns.ConcurrentSpyApi{
		ReturnValue: []cloudflare.DNSRecord{
			{Name: "example.com", Content: "1.1.1.1", ID: "1"},
			{Name: "*.example.com", Content: "1.1.1.1", ID: "2"},
			{Name: "test.example.com", Content: "1.1.1.1", ID: "3"},
		},
		UpdateApiCalls: make(chan homelab_dns.UpdateApiCall, 3),
	}
	defer close(spyApi.UpdateApiCalls)

	zoneID := "123"
	referenceHostname := "test.example.com"
	client := *homelab_dns.NewClient(spyApi, zoneID)
	app := homelab_dns.NewDefaultApp(resolver, client)

	server := homelab_dns.NewAppServer(app, referenceHostname)

	//if err := http.ListenAndServe(":5000", server); err != nil {
	//	log.Fatalf("could not listen on port 5000 %v", err)
	//}

	recorder := httptest.NewRecorder()
	request, _ := http.NewRequest(http.MethodPut, "/invoke", nil)
	server.ServeHTTP(recorder, request)

	fmt.Println(recorder.Code)
	// Output: 204
}

func TestAppCheckAndUpdateRecords(t *testing.T) {
	t.Run("check and update records", func(t *testing.T) {
		spyApp := &homelab_dns.SpyApp{}
		referenceUrl := "example.com"
		server := homelab_dns.NewAppServer(spyApp, referenceUrl)

		response := httptest.NewRecorder()
		server.ServeHTTP(response, homelab_dns.NewPutCheckAndUpdateRequest())

		homelab_dns.AssertStatus(t, response.Code, http.StatusNoContent)
		homelab_dns.AssertResponseBody(t, response.Body.String(), "")

		if len(spyApp.Calls) != 1 {
			t.Errorf("expected CheckAndUpdateRecords to be called once but got called %v times", len(spyApp.Calls))
		}

		if spyApp.Calls[0] != referenceUrl {
			t.Errorf("expected CheckAndUpdateRecords to be called with %s but got called with %s", referenceUrl, spyApp.Calls[0])
		}
	})

	t.Run("return error code", func(t *testing.T) {
		spyApp := &homelab_dns.SpyApp{ReturnError: errors.New("failed to check and update records")}
		referenceUrl := "example.com"
		server := homelab_dns.NewAppServer(spyApp, referenceUrl)

		response := httptest.NewRecorder()
		server.ServeHTTP(response, homelab_dns.NewPutCheckAndUpdateRequest())

		homelab_dns.AssertStatus(t, response.Code, http.StatusInternalServerError)
		homelab_dns.AssertResponseBody(t, response.Body.String(), "")

		if len(spyApp.Calls) != 1 {
			t.Errorf("expected CheckAndUpdateRecords to be called once but got called %v times", len(spyApp.Calls))
		}

		if spyApp.Calls[0] != referenceUrl {
			t.Errorf("expected CheckAndUpdateRecords to be called with %s but got called with %s", referenceUrl, spyApp.Calls[0])
		}
	})
}
