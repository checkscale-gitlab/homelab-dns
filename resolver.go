package homelab_dns

import (
	"errors"
	"fmt"
	"net"
)

type Lookup interface {
	LookupIP(host string) ([]net.IP, error)
}

type LookupFunc func(host string) ([]net.IP, error)

func (l LookupFunc) LookupIP(host string) ([]net.IP, error) {
	return l(host)
}

type Resolver struct {
	lookup Lookup
}

func NewResolver(lookup LookupFunc) *Resolver {
	return &Resolver{
		lookup: lookup,
	}
}

// Resolves a hostname to an IP Address according to it's lookup method.
// Use net.LookupIP for IPv4 lookups.
func (r *Resolver) Resolve(host string) (string, error) {
	results, err := r.lookup.LookupIP(host)
	if err != nil {
		return "", errors.New(fmt.Sprintf("Lookup of host %q to an IP failed", host) + err.Error())
	}
	return results[0].String(), nil
}
